package facci.pm.ta2.poo.pra1;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.FindCallback;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    ImageView imageView;
    TextView textViewNombre;
    TextView textViewPrecio;
    TextView textViewDescripcion;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        final ImageView imageView = (ImageView)findViewById(R.id.thumbnail);
        textViewNombre = findViewById(R.id.nombreD);
        textViewPrecio = findViewById(R.id.precioD);
        textViewDescripcion = findViewById(R.id.descripcionD);
        textViewDescripcion.setClickable(true);
        textViewDescripcion.setMovementMethod(LinkMovementMethod.getInstance());
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        //Acceder al object_id recibido como parámetro en la actividad.
        String objeto=  getIntent().getStringExtra("object_id");
        DataQuery query = DataQuery.get("item");
        query.getInBackground(objeto, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    //3.4
                    //Rellena el layout con las propiedades a las que accediste
                    imageView.setImageBitmap((Bitmap) object.get("image"));
                    textViewNombre.setText((String) object.get("name"));
                    textViewPrecio.setText((String) object.get("price")+"\u0024");
                    textViewDescripcion.setText((String)object.get("description"));
                }else{
                    //error
                }
            }
        });

        // FIN - CODE6



    }



}
